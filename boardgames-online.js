// Copyright 2020-2021, 2024 Peter Oliver.

// This file is part of Boardgames Online.
//
// Boardgames Online is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
//
// Boardgames Online is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Boardgames Online.  If not, see
// <https://www.gnu.org/licenses/>.

$(document).ready( function () {
    'use strict';

    const location = new URL(window.location.href);

    const bgg_url = 'https://boardgamegeek.com/';
    const bgg_xml_api_url = bgg_url + 'xmlapi2/';
    const bgg_json_api_url = 'https://api.geekdo.com/api/';
    const cors_proxy_url = location.protocol === 'file:'
          ? 'https://cors-anywhere.herokuapp.com/'
          : 'cors-proxy/';
    const max_link_count = 5;
    let page_size;

    const bga_hostname_regexp = /(?:^|\.)boardgamearena\.com$/;
    const bga_referral_code = '1e718n';

    const li_stylesheet = document.createElement('style');
    document.head.appendChild(li_stylesheet);

    const col_stylesheet = document.createElement('style');
    document.head.appendChild(col_stylesheet);

    let favicons = {};
    let collection_items;
    const outstanding_requests = new Set();

    $('input#submit-username').on("click", validate_lookup);
    $('input#submit-category').on("click", validate_browse);
    $('input#submit-mechanic').on("click", validate_browse);
    $('input#submit-hotness').on("click", validate_browse);

    const query_types = ['username', 'category', 'mechanic', 'hotness'];
    let submit_type;
    for ( const t of query_types ) {
        $('input#submit-' + t).on("click", function () { submit_type = t });
    }

    $('form#bgg-filter').on("submit", submit_bgg_filter);
    $('input#show-filters').on('click', show_filters);
    $('form#bgg-filter label.wishlist select').on(
        "change", validate_wishlist_fields
    );
    [... document.getElementById('bgg-filter').elements].filter(
        elt => elt.name !== 'page'
    ).map(
        elt => elt.addEventListener('change', reset_page)
    );

    let search_params = location.searchParams;
    for ( const [key, value] of search_params ) {
        for ( let field of document.getElementsByName(key) ) {
            field.value = value;
        }
    }
    for ( const query_type of query_types ) {
        if ( search_params.get(query_type) ) {
            if ( query_type === 'username' ) {
                validate_lookup();
            }
            else {
                validate_browse();
            }
            submit_type = query_type;
            const form = document.querySelector("form#bgg-filter");
            form.requestSubmit(form.querySelector('#submit-' + query_type));
            break;
        }
    }

    for ( const rating_type of [
        'rating',
        'average',
        'bayesaverage',
        'playingtime',
        'yearpublished',
    ]) {
        const lower_input = document.getElementById(rating_type + '-lower');
        const upper_input = document.getElementById(rating_type + '-upper');
        const lower_value = parseInt(lower_input.value);
        const upper_value = parseInt(upper_input.value);
        const lower_min = parseInt(lower_input.min);
        const upper_max = parseInt(upper_input.max);

        $('#' + rating_type + '-slider').slider({
            range: true,
            min: lower_min,
            max: upper_max,
            values: [
                isNaN(lower_value) ? lower_min : lower_value,
                isNaN(upper_value) ? upper_max : upper_value,
            ],
            step: parseFloat(lower_input.step),
            slide: function( event, ui ) {
                $('input#' + rating_type + '-lower').val(
                    ui.values[0] == lower_min ? '' : ui.values[0]
                );
                $('input#' + rating_type + '-upper').val(
                    ui.values[1] == upper_max ? '' : ui.values[1]
                );
            }
        });

        let i = 0;
        for ( const bound of ['lower', 'upper'] ) {
            $('input#' + rating_type + '-' + bound).on(
                'change',
                null,
                i++,
                function (event) {
                    $('#' + rating_type + '-slider').slider(
                        'values', event.data, this.value
                    );

                    if ( lower_min <= upper_input.value
                         && upper_input.value <= upper_max ) {
                        lower_input.max = upper_input.value;
                    }
                    if ( lower_min <= lower_input.value
                         && lower_input.value <= upper_max ) {
                        upper_input.min = lower_input.value;
                    }
                },
            );
        }
    }

    $('#listing > table').tablesorter({
        'delayInit': true
    });

    function show_filters () {
        $('div.filters').slideDown();
        $('input#show-filters').hide();
        $('input.filters').show();
    }

    function reset_page (event) {
        const page = document.getElementById('page');
        page.value = page.defaultValue;
        $('.pagination').stop().fadeOut({
            complete: function () {
                $(this).css({
                    'display': 'block',
                    'visibility': 'hidden',
                });
            }
        });
    }

    function validate_lookup () {
        const username = document.getElementById('username');
        username.required = true;
    }

    function validate_browse () {
        const username = document.getElementById('username');
        username.required = false;
    }

    function validate_wishlist_fields () {
        let yes = false;
        for ( let i = 1; i<=5; i++ ) {
            let field = document.getElementsByName('wishlist-' + i)[0];
            if ( field.options[field.selectedIndex].value === '1' ) {
                if ( yes ) {
                    field.setCustomValidity(
                        'Select “Only” for at most one wishlist.'
                    );
                    return;
                }
                yes = true;
            }
            field.setCustomValidity('');
        }
    }

    function submit_bgg_filter (event) {
        cancel_outstanding_requests();

        event.preventDefault();
        $('div#listing > table').fadeOut().slideUp();
        $('div#listing > div.error').fadeOut().slideUp();
        $('div#listing > img.spinner').fadeIn().slideDown();
        $('div#listing').slideDown();

        const search_params = new URLSearchParams();
        let history_params = new URLSearchParams();
        let submit_function;
        let subtitle;

        // // Not supported by Edge as of 2020-09
        // // https://developer.mozilla.org/en-US/docs/Web/API/SubmitEvent/submitter#Browser_compatibility
        // const submit_type =
        //       event.originalEvent.submitter.name.replace(/^submit-/, '');

        if ( submit_type === 'username' ) {
            for ( const field of event.target.elements ) {
                search_params.set(field.name, field.value);
                if (
                    ( field.tagName.toLowerCase() !== 'select'
                      || ! field.options[field.selectedIndex]['defaultSelected'] )
                        && field.name !== 'category'
                        && field.name !== 'mechanic'
                        && field.name !== 'hotness'
                        && field.value !== field.defaultValue
                        && field.type != 'submit'
                        && field.type != 'reset'
                        && field.type != 'button'
                ) {
                    history_params.set(field.name, field.value);
                }
            }

            submit_function = submit_username;
            subtitle = search_params.get(submit_type) + ' - Users';

            for (
                const elt
                of event.target.getElementsByClassName('submit-browse')
            ) {
                elt.addEventListener(
                    'click', reset_page, {
                        'once': true,
                    },
                );
            }
        }
        else if ( ['category', 'mechanic'].includes(submit_type) ) {
            const division = event.target.elements.namedItem(submit_type).value;
            search_params.set(submit_type, division);
            history_params = search_params;
            submit_function = submit_division;
            subtitle = division_name_by_id(division, submit_type) + ' - ' +
                (submit_type === 'category' ? 'Categories' : 'Mechanics');

            const other_types = new Set(query_types);
            other_types.delete(submit_type);
            for ( const t of other_types ) {
                document.getElementById('submit-' + t).addEventListener(
                    'click', reset_page, {
                        'once': true,
                    },
                );
            }
        }
        else if ( submit_type === 'hotness' ) {
            search_params.set(submit_type, true);
            history_params = search_params;
            submit_function = submit_division;
            subtitle = 'Hotness';
        }

        const title = subtitle + ' - Boardgames Online';
        document.title = title;
        history.pushState(
            {},
            title,
            '?' + history_params.toString()
        );

        submit_function(search_params, submit_type);
    }

    function division_name_by_id (id, division_type) {
        return Array.from(
            document.getElementsByName(division_type)[0].options
        ).find( o => o.value === id ).text;
    }

    function submit_division (search_params, division_type) {
        const division_id = search_params.get(division_type);
        let query;

        // The BGG JSON API treats a larger value for `showcount` as if
        // it was set to 50.
        page_size = 50;

        if ( division_type === 'hotness' ) {
            $('#listing > table > caption').html('').append(
                $('<a>').attr({
                    'href': new URL('hotness', bgg_url),
                }).text('Top 50 trending games today'),
            );

            // https://api.geekdo.com/api/hotness?geeksite=boardgame&objecttype=thing&showcount=50
            query = new URL('hotness', bgg_json_api_url);
            Object.entries({
                'geeksite': 'boardgame',
                'objecttype': 'thing',
                'showcount': page_size,
            }).forEach( ([key, value]) => query.searchParams.set(key, value) );
        }
        else {
            $('#listing > table > caption').html('').append(
                document.createTextNode(
                    'Top games ' +
                        (division_type === 'mechanic' ? 'featuring a' : 'in the') +
                        ' '
                ),
                $('<a>').attr({
                    'href': new URL(
                        ['boardgame' + division_type, division_id].map(
                            c => encodeURIComponent(c)
                        ).join('/'),
                        bgg_url
                    ),
                }).text(
                    division_name_by_id(division_id, division_type) +
                        ' ' + division_type
                ),
                $('<span>').attr({
                    'id': 'range-caption',
                }),
            );

            // https://api.geekdo.com/api/geekitem/linkeditems?ajax=1&linkdata_index=boardgame&nosession=1&objectid=2481&objecttype=property&pageid=1&showcount=25&sort=rank&subtype=boardgamecategory
            query = new URL('geekitem/linkeditems', bgg_json_api_url);
            Object.entries({
                'linkdata_index': 'boardgame',
                'nosession': '1',
                'objectid': division_id,
                'objecttype': 'property',
                'pageid': get_page_number(search_params),
                'showcount': page_size,
                'sort': 'rank',
                'subtype': 'boardgame' + division_type,
            }).forEach( ([key, value]) => query.searchParams.set(key, value) );
        }

        function send_bgg_division_request (retry_handler, zzz) {
            $.ajax({
                url: cors_proxy_url + query,
                dataType: 'json',
                success: function handle_bgg_division_response_wrapper (response_data, status, jqxhr) {
                    retry_if_required(retry_handler, zzz, response_data, status, jqxhr);
                    handle_bgg_division_response(search_params, response_data);
                },
                error: function handle_bgg_division_failure_wrapper (jqxhr, status, error) {
                    handle_bgg_failure(retry_handler, jqxhr, status, error);
                }
            });
        }

        send_bgg_division_request(send_bgg_division_request, 1000);
    }

    function handle_bgg_division_response (search_params, response_data) {
        populate_listing(search_params, response_data['items']);

        for ( let i = 0; i < col_stylesheet.sheet.cssRules.length; ++i ) {
            col_stylesheet.sheet.deleteRule(i);
        }
        col_stylesheet.sheet.insertRule(
            'td.minplayers, th.minplayers, td.maxplayers, th.maxplayers, td.playingtime, th.playingtime { display: none; }'
        );
        if ( search_params.get('hotness') ) {
            col_stylesheet.sheet.insertRule(
                'td.average, th.average { display: none; }'
            );
        }
        else {
            enable_pagination(
                get_page_number(search_params),
                response_data['config']['numitems'],
            );
            if ( response_data['config']['numitems'] > page_size ) {
                $('#range-caption').text(
                    ', ranked ' + item_range_string(
                        search_params,
                        response_data['config']['numitems']
                    )
                );
            }
        }
    }

    function retry_if_required (
        retry_handler, zzz, response_data, status, jqxhr
    ) {
        if ( jqxhr.status === 202 ) {
            if ( typeof retry_handler === 'function' ) {
                window.setTimeout(retry_handler, zzz, retry_handler, 2 * zzz);
                return;
            }
            else {
                handle_bgg_collection_failure(
                    null,
                    jqxhr,
                    status,
                    $(response_data).find('message').text()
                )
            }
        }
    }

    function submit_username (search_params, division_type) {
        const username = search_params.get('username');
        page_size = 250;

        $('#listing > table > caption').html('').append(
            $('<a>').attr({
                href: new URL(
                    'collection/user/' + encodeURIComponent(username),
                    bgg_url
                ),
                rel: 'ugc',
            }).text(username + '’s collection'),
            $('<span>').attr({
                'id': 'range-caption',
            }),
        );

        const collection_query = new URL('collection/', bgg_xml_api_url);
        const attr_map = {
            'username': 'username',
            'played': 'played',
            'rated': 'rated',
            'own': 'own',
            'prevowned': 'prevowned',
            'fortrade': 'trade',
            'want': 'want',
            'wanttobuy': 'wanttobuy',
            'preordered': 'preordered',
            'wanttoplay': 'wanttoplay',
        };
        if ( search_params.get('rated') == 1 ) {
            attr_map['rating-lower'] = 'minrating';
            attr_map['rating-upper'] = 'rating';
        }
        if ( search_params.get('bggrated') == 1 ) {
            attr_map['bayesaverage-lower'] = 'minbggrating';
            attr_map['bayesaverage-upper'] = 'bggrating';
        }

        for ( let key of Object.keys(attr_map) ) {
            let value = search_params.get(key);
            if ( value !== null && value !== '' ) {
                collection_query.searchParams.set(attr_map[key], value);
            }
        }
        for ( let i = 1; i<=5; i++ ) {
            if ( search_params.get('wishlist-' + i) === '1' ) {
                collection_query.searchParams.set('wishlistpriority', i);
                break;
            }
        }
        // We always need stats now that we seek to display them.
        collection_query.searchParams.set('stats', 1);

        function send_bgg_collection_request (retry_handler, zzz) {
            $.ajax({
                url: collection_query,
                dataType: 'xml',
                success: function handle_bgg_collection_response_wrapper (response_data, status, jqxhr) {
                    retry_if_required(retry_handler, zzz, response_data, status, jqxhr);

                    handle_bgg_collection_response(search_params, response_data);
                },
                error: function handle_bgg_collection_failure_wrapper (jqxhr, status, error) {
                    handle_bgg_collection_failure(retry_handler, jqxhr, status, error);
                }
            });
        }

        send_bgg_collection_request(send_bgg_collection_request, 1000);
    }

    function handle_bgg_collection_response (search_params, response_data) {
        const listed_item_ids = new Set();
        const items = [];

        $(response_data).find('items > item').each(function () {
            // Filter wishlist priorities ourselves:
            const status = $(this).find('status');
            if (
                status.attr('wishlist') === '1'
                    && search_params.get(
                        'wishlist-' + status.attr('wishlistpriority')
                    ) === '0'
            ) {
                return;
            }

            // Filter presence of Geek Rating ourselves:
            const stats = $(this).find('stats');
            const rating = stats.children('rating');
            if ( search_params.get('bggrated') === '0' ) {
                if ( rating.children('bayesaverage').attr('value') != 0 ) {
                    return;
                }
            }
            else if ( search_params.get('bggrated') === '1' ) {
                if ( rating.children('bayesaverage').attr('value') == 0 ) {
                    return;
                }
            }

            // Filter ratings ourselves.
            // `average` is always filtered client-side (due to API).
            // `rating` is server side if `rated=1` and client side otherwise.
            // `bayesaverage` is server side if `bggrated=1` and client
            //                side otherwise.
            for ( const rating_type of [
                'rating',
                'average',
                'bayesaverage',
                'playingtime',
                'yearpublished',
            ] ) {
                let lower = parseInt(
                    search_params.get(rating_type + '-lower')
                );
                if ( isNaN(lower) ) {
                    lower = Number.NEGATIVE_INFINITY;
                }

                let upper = parseInt(
                    search_params.get(rating_type + '-upper')
                );
                if ( isNaN(upper) ) {
                    upper = Number.POSITIVE_INFINITY;
                }

                let score;
                switch ( rating_type ) {
                case 'rating':
                    score = Number(rating.attr('value'));
                    break;
                case 'playingtime':
                    score = Number(stats.attr('playingtime'));
                    break;
                case 'yearpublished':
                    score = Number($(this).children('yearpublished').text());
                    break;
                default:
                    score = Number(rating.children(rating_type).attr('value'));
                }

                if ( rating_type === 'bayesaverage' && score === 0 ) {
                    score = NaN;
                }

                if ( score < lower || upper < score ) {
                    return;
                }
            }

            let playercount = Number(search_params.get('playercount'));
            if ( playercount != 0 ) {
                if ( playercount <  Number(stats.attr('minplayers'))
                     || Number(stats.attr('maxplayers')) < playercount ) {
                    return;
                }
            }

            const objectid = Number($(this).attr('objectid'));
            if ( listed_item_ids.has(objectid) ) {
                return;
            }
            else {
                listed_item_ids.add(objectid);
            }

            items.push({
                'objectid': objectid,
                'name': $(this).children('name').text(),
                'average': rating.children('average').attr('value'),
                'subtype': $(this).attr('subtype'),
                'minplayers': stats.attr('minplayers'),
                'maxplayers': stats.attr('maxplayers'),
                'minplaytime': stats.attr('minplaytime'),
                'maxplaytime': stats.attr('maxplaytime'),
                'playingtime': stats.attr('playingtime'),
                'yearpublished': $(this).children('yearpublished').text(),
                'images': {
                    'thumb': $(this).children('thumbnail').text(),
                },
            });
        });

        collection_items = items;
        populate_collection(search_params);

        // Unhide columns available returned by BGG for collections but
        // not for categories, etc.
        for ( let i = 0; i < col_stylesheet.sheet.cssRules.length; ++i ) {
            col_stylesheet.sheet.deleteRule(i);
        }
        col_stylesheet.sheet.insertRule(
            'td.position, th.position, td.rank, th.rank { display: none; }'
        );
    }

    function populate_collection (search_params) {
        const page = get_page_number(search_params);
        populate_listing(
            search_params,
            collection_items.slice(
                (page - 1) * page_size,
                page * page_size
            ),
        );
        enable_pagination(page, collection_items.length);
        if ( collection_items.length > page_size ) {
            $('#range-caption').text(
                ', items ' +
                    item_range_string(search_params, collection_items.length)
            );
        }
    }

    function populate_listing (search_params, items) {
        $('div#listing > img.spinner').fadeOut().slideUp();
        $('div#listing > table > tbody').empty();

        const page = get_page_number(search_params);
        let i =  (page - 1) * page_size + 1;

        for ( const item of items ) {
            const page_url = new URL(
                item['href'] || (encodeURIComponent(item['subtype']) + '/'
                                 + encodeURIComponent(item['objectid']) + '/-'),
                bgg_url
            );

            let delta_class, delta_title, delta_inner, delta_span;
            if ( 'delta' in item ) {
                if ( item['delta'] === null ) {
                    delta_class = 'new';
                    delta_inner = ['🆕'];
                    delta_title = 'New entry to the top 50 today';
                }
                else {
                    if ( item['delta'] > 0 ) {
                        delta_class = 'rising';
                        delta_inner = [$('<span/>').text('▴')];
                        delta_title = 'Rose this many places since yesterday';
                    }
                    else {
                        delta_class = 'falling';
                        delta_inner = [$('<span/>').text('▾')];
                        delta_title = 'Fell this many places since yesterday';
                    }
                    delta_inner.push('&nbsp;', Math.abs(item['delta']));
                }
                delta_span = $('<span/>').attr({
                    'class': delta_class,
                    'title': delta_title,
                }).append(delta_inner)
            }

            const rank_url = new URL(
                '/browse/boardgame?sort=rank&rankobjecttype=subtype&rankobjectid=1',
                bgg_url
            );
            rank_url.searchParams.append('rank', item['rank'])
            rank_url.hash = item['rank'];
            const rank_a = $('<a/>').attr({
                href: rank_url,
                rel: 'ugc',
            }).text(item['rank']);

            let summary_td;
            if ( 'maxplayers' in item ) {
                summary_td = $('<td/>').attr({
                    class: 'summary',
                }).text(
                    ( item['maxplayers'] == 1
                      ? `1 player`
                      : item['minplayers'] == item['maxplayers']
                      ? `${item['minplayers']} players`
                          : `${item['minplayers']}–${item['maxplayers']} players` ) +
                        ', ' +
                        ( item['minplaytime'] == item['maxplaytime']
                          ? `${item['playingtime']} minutes.`
                          : `${item['minplaytime']}–${item['maxplaytime']} minutes.` )
                );
            }
            else {
                summary_td = $('<td/>').attr({
                    class: 'summary',
                }).append(i);
                if ( 'delta' in item ) {
                    summary_td.append(' (', delta_span.clone(), ')')
                }
                summary_td.append(' &mdash; ');
                if ( 'rank' in item ) {
                    summary_td.append(
                        'Ranked&nbsp;', rank_a.clone(), '&nbsp;overall.'
                    );
                }
                else {
                    summary_td.append('Unranked overall.');
                }
            }

            let thumbnail_url;
            try {
                thumbnail_url = item['images']['thumb']
                    || item['images']['mediacard']['src'];
            }
            catch ( e ) {
                if ( ! (e instanceof TypeError) ) {
                    throw e;
                }
            }

            $('div#listing > table > tbody').append(
                $('<tr/>').attr({
                    id: 'objectid-' + item['objectid']
                }).append(
                    $('<td/>').attr({
                        'class': 'position integer',
                    }).append(
                        i++,
                        $('<br/>'),
                        delta_span !== undefined ? delta_span : [],
                    ),
                    $('<td/>').attr({
                        class: 'thumbnail'
                    }).append(
                        thumbnail_url !== undefined
                            ? $('<a/>').attr({
                                href: page_url,
                                rel: 'ugc',
                              }).append(
                                  $('<img/>').attr({
                                      src: thumbnail_url,
                                  })
                              )
                            : '',
                    ),
                    $('<th/>').attr({
                        class: 'name'
                    }).append(
                        $('<a/>').attr({
                            href: page_url,
                            rel: 'ugc',
                        }).text(item['name'])
                    ),
                    summary_td,
                    $('<td/>').attr({
                        class: 'rank integer'
                    }).append(rank_a),
                    $('<td/>').attr({
                        class: 'average decimal',
                    }).append(
                        $('<a/>').attr({
                            href: page_url + '/stats',
                            rel: 'ugc',
                        }).text(parseFloat(item['average']).toFixed(1))
                    ),
                    $('<td/>').attr({
                        class: 'minplayers integer',
                    }).text(item['minplayers']),
                    $('<td/>').attr({
                        class: 'maxplayers integer',
                    }).text(item['maxplayers']),
                    $('<td/>').attr({
                        class: 'playingtime integer',
                        title: 'minutes'
                    }).text(item['playingtime']),
                    $('<td/>').attr({
                        class: 'yearpublished integer',
                    }).append(
                        $('<a/>').attr({
                            href: page_url + '/versions',
                            rel: 'ugc',
                        }).text(item['yearpublished'])
                    ),
                    $('<td/>').attr({
                        class: 'links'
                    }).append(
                        $('<img/>').attr({
                            src: 'images/spinner.gif',
                            class: 'spinner'
                        })
                    )
                )
            );
            fetch_bgg_links(item['objectid']);
        }

        $('div#listing > table').trigger('update');
        $('div#listing > table').fadeIn().slideDown();
    }

    function enable_pagination (page, item_count) {
        if ( item_count <= page_size ) {
            return;
        }

        const page_count = Math.ceil(item_count / page_size)
        const page_input = $('input#page');

        if ( page > page_count ) {
            page = page_count;
            page_input.val(page);
        }

        $('.pagination').stop().css({
            'display': 'none',
            'visibility': 'visible',
        }).fadeIn();
        page_input.attr({'max': page_count});
        $('#page-count').text(page_count);

        const page_first = $('input#page-first');
        const page_prev = $('input#page-prev');
        const page_next = $('input#page-next');
        const page_last = $('input#page-last');
        page_first.off('click');
        page_prev.off('click');
        page_next.off('click');
        page_last.off('click');
        if ( page > page_input.attr('min') ) {
            page_first.prop('disabled', false);
            page_first.on(
                'click',
                function on_click_first (event) {
                    page_input.val(page_input.attr('min'));
                    update_page(search_params, page_input.val());
                }
            );
            page_prev.prop('disabled', false);
            page_prev.on(
                'click',
                function on_click_prev (event) {
                    page_input.val(Number(page_input.val()) - 1);
                    update_page(search_params, page_input.val());
                }
            );
        }
        else {
            page_first.prop('disabled', true);
            page_prev.prop('disabled', true);
        }
        if ( page < page_input.attr('max') ) {
            page_next.prop('disabled', false);
            page_next.on(
                'click',
                function on_click_next (event) {
                    page_input.val(Number(page_input.val()) + 1);
                    update_page(search_params, page_input.val());
                }
            );
            page_last.prop('disabled', false);
            page_last.on(
                'click',
                function on_click_last (event) {
                    page_input.val(page_input.attr('max'));
                    update_page(search_params, page_input.val());
                }
            );
        }
        else {
            page_next.prop('disabled', true);
            page_last.prop('disabled', true);
        }
    }

    function update_page (search_params, page) {
        cancel_outstanding_requests();

        search_params.set('page', page);
        $("#listing")[0].scrollIntoView();

        const params = new URL(window.location).searchParams;
        params.set('page', page);
        history.pushState(
            {},
            document.title,
            '?' + params.toString()
        );

        for ( const query_type of query_types ) {
            if ( search_params.get(query_type) ) {
                if ( query_type === 'username' ) {
                    populate_collection(search_params);
                }
                else {
                    submit_division(search_params, query_type);
                }
            }
        }
    }

    function handle_bgg_collection_failure (retry_handler, jqxhr, status, error) {
        if ( jqxhr.status === 0
             && typeof retry_handler === 'function' ) {
            // This is likely a 202 Accepted response that we can't see
            // because of a missing CORS header.  Retry once only.
            //
            // Some kind of delay might turn out to be necessary here.
            retry_handler();
            return;
        }
        else {
            handle_bgg_failure(retry_handler, jqxhr, status, error);
        }
    }

    function handle_bgg_failure (retry_handler, jqxhr, status, error) {
        $('div#listing > img.spinner').fadeOut().slideUp();
        $('div#listing p.error').text(
            'Could not fetch listing from BoardGameGeek: '
                + status + ': '
                + (error == '' ? 'unknown' : error)
        );
        $('div#listing > div.error').fadeIn().slideDown();
    }

    function fetch_bgg_links (objectid) {
        let url = new URL('geekitem/linkeditems', bgg_json_api_url);
        url.searchParams.append('linkdata_index', 'weblink');
        url.searchParams.append('categoryfilter', '2715');
        url.searchParams.append('showcount', max_link_count + 1);
        url.searchParams.append('objecttype', 'thing');
        url.searchParams.append('objectid', objectid);
        $.ajax({
            url: cors_proxy_url + url,
            dataType: 'json',
            beforeSend: add_outstanding_request,
            success: function handle_bgg_links_response_wrapper (links) {
                handle_bgg_links_response(objectid, links);
            },
            error: function handle_bgg_links_failure_wrapper (jqxhr, status, error) {
                handle_bgg_links_failure(objectid, jqxhr, status, error);
            },
            complete: delete_outstanding_request,
        });
    }

    function handle_bgg_links_response (objectid, links) {
        if ( links.items.length > 0 ) {
            $('tr#objectid-' + objectid + ' img.spinner').slideUp();
            let ul = $('<ul/>').addClass('links').hide();
            if ( links.items.length > max_link_count ) {
                let a = $('tr#objectid-' + objectid + ' > .name > a');
                let slug = a.text().toLowerCase().replace(/ /g, '-');

                links.items[links.items.length - 1] = {
                    name: 'More…',
                    url: a.attr('href') + '/' + encodeURIComponent(slug) +
                        '/mentions/links?categoryfilter=2715',
                };
            }
            for (let i in links.items) {
                if ( i > max_link_count + 1 ) {
                    break;
                }

                const item = links.items[i];
                let link_url;
                let favicon_url;
                try {
                    link_url = new URL(item.url);
                    if ( link_url.protocol !== 'http:'
                         && link_url.protocol !== 'https:' ) {
                        throw 'UnsafeScheme';
                    }
                    if ( bga_hostname_regexp.test(link_url.hostname) ) {
                        link_url.searchParams.set('sp', bga_referral_code);
                    }
                    favicon_url = link_url.hostname == 'tabletopia.com'
                        ? new URL('https://tabletopia.com/Content/Images/favicon.png')
                        : new URL('/favicon.ico', link_url);
                }
                catch ( e ) {
                    if ( e instanceof TypeError
                         || e === 'UnsafeScheme' ) {
                        link_url = new URL(
                            Number(item.objectid),
                            'https://boardgamegeek.com/weblink/'
                        );
                    }
                    else {
                        throw e;
                    }
                }

                const span = $('<span/>').append(
                    $('<a/>').attr({
                        'href': link_url,
                        'rel': 'ugc',
                    }).text(
                        item.name
                    )
                );
                const flex = $('<span/>').append(span);
                const li = $('<li/>').append(flex);

                if ( favicon_url !== undefined ) {
                    span.append(
                        $('<span class="advice"/>').text(
                            ' [' +
                                favicon_url.hostname.replace(/^www\./, '')
                                + ']'
                        )
                    );
                }
                if ( Object.prototype.hasOwnProperty.call(item, 'objectid') ) {
                    flex.append(
                        $('<a/>').attr({
                            class: 'edit',
                            title: 'Make a correction to this link…',
                            href: bgg_url + 'item/correction/weblink/' +
                                encodeURIComponent(item.objectid),
                            rel: 'ugc',
                        }).text(
                            '✏️'
                        ),
                    );
                }

                if ( favicon_url instanceof URL ) {
                    const li_class = 'site_' + favicon_url.host.replace(
                        /[.:]/g, '_'
                    );
                    li.addClass(li_class);

                    if ( ! Object.prototype.hasOwnProperty.call(favicons,
                                                                li_class) ) {
                        favicons[li_class] = $('<img>').attr({
                            src: favicon_url
                        }).on(
                            'load',
                            function (event) {
                                li_stylesheet.sheet.insertRule(
                                    'li.' + li_class + ' {' +
                                        'list-style: none;' +
                                        'position: relative;' +
                                        '}'
                                );
                                li_stylesheet.sheet.insertRule(
                                    'li.' + li_class + ':before {' +
                                        'content: "";' +
                                        'background-image: ' + css_url(favicon_url) + ';' +
                                        'background-size: contain;' +
                                        'background-repeat: no-repeat;' +
                                        'background-position: center;' +
                                        'width: 16px;' +
                                        'height: 16px;' +
                                        'position: absolute;' +
                                        'left: -2.5ch;' +
                                        'top: +3px;' +
                                        '}'
                                );
                            }
                        );
                    }
                }
                ul.append(li);
            }
            if ( links.items.length <= max_link_count ) {
                ul.append(
                    $('<li/>').addClass('add').append(
                        $('<span/>').append(
                            $('<span/>'),
                            $('<a/>').attr({
                                class: 'edit',
                                title: 'Add a missing link…',
                                href: bgg_url + 'item/create/weblink' +
                                    '?src_objecttype=thing' +
                                    '&src_objectid=' + encodeURIComponent(objectid),
                                rel: 'ugc',
                            }).text(
                                '+'
                            )
                        )
                    )
                );
            }
            $('tr#objectid-' + objectid + ' > td.links').append(ul);
            ul.fadeIn().slideDown();
        }
        else {
            $('tr#objectid-' + objectid)
                .fadeOut()
                .children()
                .animate({ paddingTop: 0, paddingBottom: 0 }, 400)
                .wrapInner('<div/>')
                .children()
                .slideUp(
                    400,
                    function () {
                        $(this).closest('tr').remove()
                    }
                );
        }
    }

    function handle_bgg_links_failure (objectid, jqxhr, status, error) {
        $('tr#objectid-' + objectid + ' img.spinner').slideUp();
        $('tr#objectid-' + objectid + ' > td.links').append(
            $('<span/>').attr({
                title: status + ': ' + (error == '' ? 'unknown' : error)
            }).text(
                '⚠'
            )
        );
    }

    function add_outstanding_request (jqhxr) {
        outstanding_requests.add(jqhxr);
    }
    function delete_outstanding_request (jqxhr) {
        outstanding_requests.delete(jqxhr);
    }
    function cancel_outstanding_requests () {
        for ( const jqxhr of outstanding_requests.values() ) {
            jqxhr.abort();
        }
        outstanding_requests.clear();
    }

    function css_url (url) {
        return 'url("' + encodeURI(url) + '")';
    }

    function item_range_string (search_params, max) {
        const page = get_page_number(search_params);
        const first = (page - 1) * page_size + 1;
        const last = (page * page_size);
        return first + '–' + ( max < last ? max : last);
    }

    function get_page_number (search_params) {
        return search_params.has('page')
            ? parseInt(search_params.get('page'))
            : 1;
    }
});
