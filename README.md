# [Boardgames Online](https://www.mavit.org.uk/boardgames-online/)

Discover where to play your collection of boardgames via the Internet.

## What’s this all about?

During Covid-19 lockdown, lots of people have been playing boardgames remotely with friends using voice chat.

The first thing you have to do is to pick a game to play.  Many are available to play online, but it can be tiresome browsing through a site such as [Tabletopia](https://tabletopia.com/games) looking for a game that you know well enough to teach to other people without being able to physically wave the components at them.

Using [Boardgames Online](https://www.mavit.org.uk/boardgames-online/), you can get a listing of online versions of games that you already know, provided that those games are in your collection on [BoardGameGeek](https://boardgamegeek.com/).

## Data

All data sourced from [BoardGameGeek](https://boardgamegeek.com/).  If you notice that digital editions of games are missing here, please [add them to BGG using the Online Play link type](https://boardgamegeek.com/wiki/page/Link_Submissions).

## Hacking

[CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) is needed to access BGG’s JSON API, but it supplies no `Access-Control-Allow-Origin` HTTP header, so you’ll need to use a proxy.  Change the value of `cors_proxy_url` in `boardgames-online.js`.  For testing, I suggest <https://cors-anywhere.herokuapp.com/>.

## Licence

Copyright 2020-2021, 2024 Peter Oliver.

Boardgames Online is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Boardgames Online is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Boardgames Online.  If not, see <https://www.gnu.org/licenses/>.

## Gratuitous Plug

I co-organise a casual board game social in Birmingham, UK, called [Afternoon Play](https://afternoonplay.co.uk/).  If you’re in the area, you should come.
